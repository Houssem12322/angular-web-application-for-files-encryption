import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { FileResult, FileEncryptionService } from './services/file-encryption.service';
import { MatDialog } from '@angular/material/dialog';
import { ResultListComponent } from './result-list/result-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  filesEncryptionForm: FormGroup;
  filesToEncrypt: File[]  = [];
  keyFile: File;
  isProcessing: boolean = false;
  encryptedDecryptedFiles: FileResult[] = [];


  constructor (
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private iconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private filesEncryption: FileEncryptionService
    ) {

    this.filesEncryptionForm = this.formBuilder.group({
      filesToEncrypt: ['', Validators.required],
      keyFile: ['', Validators.required]
    });

    this.iconRegistry.addSvgIconSet(this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg-icons/icons.svg'));
  }

  addFilesToEncrypt (event) {
    let files:FileList = event.target.files;
    let length: number = files.length;
    for (let i=0; i<length; i++) {    

      if (this.filesToEncrypt.every((currentFile) => {
        return (currentFile.name !== files[i].name)
               || (currentFile.size !== files[i].size)
               || (currentFile.type !== files[i].type);
      })) {
        this.filesToEncrypt.push(files[i]);
      }
    }
    event.target.value = ''; 
    if (this.filesToEncrypt.length > 0){
      this.filesEncryptionForm.patchValue({ 'filesToEncrypt': 'not empty' });
    }
      
  }


  cancelFile (file: File) {
    const index: number = this.filesToEncrypt.indexOf(file);

    
    if (index >= 0) {
      this.filesToEncrypt.splice(index, 1);
    }

    if (this.filesToEncrypt.length === 0) {
      this.filesEncryptionForm.patchValue({'filesToEncrypt': ''});
    }
  }

  addFileAsKey (event) {
    this.keyFile = event.target.files[0];
    event.target.value = '';
     this.filesEncryptionForm.patchValue({ 'keyFile': 'not Empty' });
  }

  cancelKeyFile () {
    this.keyFile = null;
    this.filesEncryptionForm.patchValue({ 'keyFile': '' });

  }

  encrypt () {
    this.isProcessing = true;
    this.filesEncryption.encryptFiles(this.filesToEncrypt, this.keyFile).then((encryptedFiles: FileResult[]) => {
      this.isProcessing = false;
      this.encryptedDecryptedFiles = encryptedFiles;
      this.openResultList();
    })
    .catch (e => {
      this.isProcessing = false;
      this.openResultList(e.message);
    });
  }

  decrypt () {
    this.isProcessing = true;
    this.filesEncryption.decryptFiles(this.filesToEncrypt, this.keyFile).then((decryptedFiles: FileResult[]) => {
      this.isProcessing = false;
      this.encryptedDecryptedFiles = decryptedFiles;
      this.openResultList();
    })
    .catch(e => {
      this.isProcessing = false;
      this.openResultList(e.message);
    });

  }
  openLastEncryption (): void {
    if (this.encryptedDecryptedFiles.length < 1) {
      this.openResultList('No file has been processed')
    } else {
      this.openResultList();
    }

  }

  openResultList (errorMessage?: string): void {

    let dialogARIALLabel: string = "List of the processed Files"
    if (errorMessage) {
      this.dialog.open(ResultListComponent, {
        minWidth: '30vw',
        data: errorMessage,
        disableClose: true,
        ariaLabel: dialogARIALLabel
      });

    } else {
      this.dialog.open(ResultListComponent, {
        data: this.encryptedDecryptedFiles,
        minWidth: '30vw',
        disableClose: true,
        ariaLabel: dialogARIALLabel
      });
    }
    

  }
}

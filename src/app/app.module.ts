import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { MatInputModule } from '@angular/material/input'; 
import { MatButtonModule } from '@angular/material/button'; 
import { MatIconModule } from '@angular/material/icon'; 
import { MatCardModule } from '@angular/material/card'; 
import { MatChipsModule } from '@angular/material/chips'; 
import { MatExpansionModule } from '@angular/material/expansion'; 
import { MatDividerModule } from '@angular/material/divider'; 
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list'; 
import { MatDialogModule } from '@angular/material/dialog'; 

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FileEncryptionService } from './services/file-encryption.service';
import { ResultListComponent } from './result-list/result-list.component';


@NgModule({
  declarations: [
    AppComponent,
    ResultListComponent
  ],
  imports: [
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatChipsModule,
    MatExpansionModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatListModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule, 
  ],
  providers: [
    FileEncryptionService,
  ],
  entryComponents: [
    ResultListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

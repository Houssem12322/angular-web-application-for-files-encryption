import { Component, Inject, SecurityContext } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FileResult } from '../services/file-encryption.service'
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.scss']
})
export class ResultListComponent {

  filesSafeUrls: SafeUrl[] = [];
  errorMessage: string = null;
  constructor(
    public dialogRef: MatDialogRef<ResultListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: FileResult[] | string,
    private sanitazer: DomSanitizer) {
      if (typeof data === 'string') {
        this.errorMessage = data;

      } else {
        this.filesSafeUrls = data.map((element): SafeUrl => {
          if (element.file === null){
            return '';
  
          } else {
            return this.sanitazer.bypassSecurityTrustUrl(window.URL.createObjectURL(element.file));
          }
          
        });
      }
      
    }

  closeDialog(): void {

    this.filesSafeUrls.forEach(element => {
      window.URL.revokeObjectURL(this.sanitazer.sanitize(SecurityContext.URL, element));  
    });
    
    this.dialogRef.close();
  }

  saveLanched (event: Event) {

    let element: HTMLElement = event.target as HTMLElement;
    let parent: HTMLElement = element.parentNode as HTMLElement;

    if (element.tagName !== 'A')
    {
      if (parent.getAttribute("disabled") !== "true") {
        parent.setAttribute('style', 'background-color: #b0bec5;');
      }
      
    } else {
      if (element.getAttribute("disabled") !== "true") {
        element.setAttribute('style', 'background-color: #b0bec5;');
      }
      
    }  
  }

}

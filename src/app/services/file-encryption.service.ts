import { Injectable } from '@angular/core';

export interface FileResult {
  file: File;
  state: String;
}

@Injectable({
  providedIn: 'root'
})
export class FileEncryptionService {

  constructor() { }

  
  async encryptFiles (filesToEncrypt: File[], keyFile: File): Promise<FileResult[]> {
    try {

      const key = await this.importAESKey(keyFile);
      return await Promise.all(filesToEncrypt.map(async (file) => {
        try {
          const encryptedBuffer: ArrayBuffer = await this.encryptFile(file, key);
          return Promise.resolve({file: new File([encryptedBuffer], `encrypted-${file.name}`, {type: file.type}), state: 'done'});

        } catch (singleFileError) {

          return Promise.resolve({file: null, state: singleFileError.message});
        }
      }));

    } catch (allEncryptionError) {
      return Promise.reject(allEncryptionError);
      
    }

  }

  async decryptFiles (filesToDecrypt: File[], keyFile: File) {
    try {
      const key = await this.importAESKey(keyFile);
      return await Promise.all(filesToDecrypt.map(async (file) => {
        try {

          let decryptedBuffer: ArrayBuffer = await this.decryptFile(file, key);
          return Promise.resolve({file: new File([decryptedBuffer], `decrypted-${file.name}`, {type: file.type}), state: 'done'});

        } catch (singleFileError) {

          return Promise.resolve({file: null, state: singleFileError.message});
        }
      }));

    } catch (allDecryptionError) {
      return Promise.reject(allDecryptionError);
    }
  }

  async encryptFile (fileToEncrypt: File, key: CryptoKey): Promise<ArrayBuffer> {

    let bufferToencrypt: ArrayBuffer;

    try {
      bufferToencrypt = await this.readFile(fileToEncrypt);
    } catch (error) {
      return Promise.reject(error);
    }

    const iv: Uint8Array = window.crypto.getRandomValues(new Uint8Array(12)); 
    let encryptedBuffer: ArrayBuffer;

    try {     
      encryptedBuffer = await window.crypto.subtle.encrypt({name: "AES-GCM", iv: iv}, key, bufferToencrypt);
    } catch (error) {
      return Promise.reject(error);
    }

    let encryptedArrayIv: Uint8Array = new Uint8Array(encryptedBuffer.byteLength + iv.length);
    encryptedArrayIv.set(iv, 0);
    encryptedArrayIv.set(new Uint8Array(encryptedBuffer), 12);
    return Promise.resolve(encryptedArrayIv.buffer);
  }

  async decryptFile (fileToDecrypt: File, key: CryptoKey): Promise<ArrayBuffer> {

    let bufferToDecrypt: ArrayBuffer;

    try {
      bufferToDecrypt = await this.readFile(fileToDecrypt);
    } catch (error) {    
      return Promise.reject(error);
    }

    const iv: Uint8Array = new Uint8Array(bufferToDecrypt.slice(0,12));
    let decryptedBuffer: ArrayBuffer; 

    try {
      decryptedBuffer = await window.crypto.subtle.decrypt({name: "AES-GCM", iv: iv}, key, bufferToDecrypt.slice(12));
    } catch (error) {
      return Promise.reject(new Error(`'${fileToDecrypt.name}' was not encrypted with this key`));
    }

    return Promise.resolve(decryptedBuffer);

  }



  readFile (file: File): Promise<any> {
    

    return new Promise ((resolve, reject) => {
      let fileReader: FileReader = new FileReader();
      fileReader.onloadend = () => {
        resolve(fileReader.result);
      };
  
      fileReader.onerror = () => {
        fileReader.abort();
        reject (new Error (`Could not read '${file.name}'.<br/>
                           The file may be too large or the operation is not allowed.`));
      };
  
      fileReader.readAsArrayBuffer(file);
    });
    
  }

  async hashFile (file: File): Promise<ArrayBuffer> {

    try {

      const arrayBufferFile = await this.readFile(file)
      const hash = await crypto.subtle.digest('SHA-256', arrayBufferFile);
      return Promise.resolve(hash);

    } catch (error) {

      return Promise.reject(error);
    }
    
  }

  async importAESKey(keyFile: File): Promise<CryptoKey> {

    try {
      const hash: ArrayBuffer = await this.hashFile(keyFile);
      return window.crypto.subtle.importKey(
        "raw",
        hash,
        "AES-GCM",
        true,
        ["encrypt", "decrypt"]
      );
    } catch (error) {
      return Promise.reject(error);
    }
    
  }
}